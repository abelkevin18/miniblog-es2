package com.mitocode.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;
import org.primefaces.PrimeFaces;

import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

	@Inject
	private IUsuarioService service;

	private Usuario usuario;
	private List<Usuario> lista;

	private String headerModal;
	private String busqueda;
	private String txtclave;

	private String txtnuevaClave;
	private String txtnuevaClave2;

	private String statusbtnAceptar;

	@PostConstruct
	public void init() {
		this.usuario = new Usuario();
		this.listar("");

		this.statusbtnAceptar = "true";

	}

	public void listar(String usuario) {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			this.lista = this.service.listarFiltrado(usuario);
			if (this.service.listarFiltrado(usuario).size() == 0) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia!",
						"No se encontraron coincidencias"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void buscarUsuario() {
		listar(this.busqueda);

	}

	public void mostrarUsuario(Usuario u) {
		this.usuario = u;
		this.headerModal = "Modificando Usuario: " + u.getUsuario();
	}

	public void verificarClave() {

		String txtclave = this.txtclave;

		FacesContext context = FacesContext.getCurrentInstance();
		if (BCrypt.checkpw(txtclave, this.usuario.getContrasena())) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje:", "Contrase�a correcta."));
			this.statusbtnAceptar = "false";
		} else {
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia!", "Contrase�a err�nea!"));
			this.statusbtnAceptar = "true"; // en caso de que pulse m�s de una vez y la contrase�a no coincida
		}

	}

	public void modificarClave() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (this.txtnuevaClave.equals("")) {
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia!", "Escriba la nueva contrase�a"));
		} else {
			if (this.txtnuevaClave.equals(txtnuevaClave2)) {

				String pw_hash = BCrypt.hashpw(this.txtnuevaClave, BCrypt.gensalt());
				this.usuario.setContrasena(pw_hash);
				Usuario usuarioEditado = this.usuario;

				try {
					this.statusbtnAceptar = "true";
					service.modificar(usuarioEditado);
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje:",
							"Contrase�a modificada correctamente."));
					PrimeFaces current = PrimeFaces.current();
					current.executeScript("PF('wdlg').hide();");
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

			} else {
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia!", "Las contrase�as no coinciden"));
			}
		}

	}

	/*
	 * getters and setters
	 */

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	public String getHeaderModal() {
		return headerModal;
	}

	public void setHeaderModal(String headerModal) {
		this.headerModal = headerModal;
	}

	public String getBusqueda() {
		return busqueda;
	}

	public void setBusqueda(String busqueda) {
		this.busqueda = busqueda;
	}

	public String getTxtclave() {
		return txtclave;
	}

	public void setTxtclave(String txtclave) {
		this.txtclave = txtclave;
	}

	public String getStatusbtnAceptar() {
		return statusbtnAceptar;
	}

	public void setStatusbtnAceptar(String statusbtnAceptar) {
		this.statusbtnAceptar = statusbtnAceptar;
	}

	public String getTxtnuevaClave() {
		return txtnuevaClave;
	}

	public void setTxtnuevaClave(String txtnuevaClave) {
		this.txtnuevaClave = txtnuevaClave;
	}

	public String getTxtnuevaClave2() {
		return txtnuevaClave2;
	}

	public void setTxtnuevaClave2(String txtnuevaClave2) {
		this.txtnuevaClave2 = txtnuevaClave2;
	}


}
