package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Usuario;

public interface IUsuarioService {

	Usuario login(Usuario us);
	List <Usuario> listarFiltrado(String letras) throws Exception;
	public Integer modificar(Usuario t) throws Exception;
}
